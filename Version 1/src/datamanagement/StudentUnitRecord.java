package datamanagement;

public class StudentUnitRecord implements IStudentUnitRecord {
    private Integer sid;
    private String uc;
    private float a1, a2, ex;
    //Set student's units to record 
    public StudentUnitRecord(Integer id, String code, float asg1, float asg2,float exam) {
        this.sid = id;
	this.uc = code;
	this.setAsg1(asg1);
	this.setAsg2(asg2);
	this.setExam(exam);
    }
    //Get student id
    public Integer getStudentID() {
	return sid;
    }
    //Get unit code
    public String getUnitCode() {
	return uc;
    }
    //Set assignment1 mark
    public void setAsg1(float a1) {
	if (a1 < 0 ||   a1 > UnitManager.UM().getUnit(uc).getAsg1Weight()) {
            throw new RuntimeException("Mark cannot be less than zero or greater than assessment weight");
        }
	this.a1 = a1;
    }
    //Get assignment1 mark
    public float getAsg1() {
        return a1;
    }
    //Set assignment2 mark
    public void setAsg2(float a2) {
	if (a2 < 0 || a2 > UnitManager.UM().getUnit(uc).getAsg2Weight()) {
            throw new RuntimeException("Mark cannot be less than zero or greater than assessment weight");
        }
	this.a2 = a2;
    }
    //Get assignment2 mark
    public float getAsg2() {
	return a2;
    }
    //Set exam mark
    public void setExam(float ex) {
	if (ex < 0 ||  ex > UnitManager.UM().getUnit(uc).getExamWeight()) {
            throw new RuntimeException("Mark cannot be less than zero or greater than assessment weight");
        }
        this.ex = ex;
    }
    //Get exam mark
    public float getExam() {
        return ex;
    }
    //Get Total mark
    public float getTotal() {
        return a1 + a2 + ex;
    }
}
