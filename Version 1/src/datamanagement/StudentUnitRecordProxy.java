package datamanagement;
//Override methods for get and set student id, assignment1, assignment2 and final exam marks
public class StudentUnitRecordProxy implements IStudentUnitRecord {
    private final Integer studentID;
    private final String unitCode;
    private final StudentUnitRecordManager mngr;
    public StudentUnitRecordProxy( Integer id, String code ) 
    {
        this.studentID = id;this.unitCode = code;
        this.mngr = StudentUnitRecordManager.instance();
    }
    @Override
    public Integer getStudentID() { return studentID;}
    
    @Override
    public String getUnitCode() { return unitCode; }

    @Override
    public void setAsg1(float mark) {
        mngr.getStudentUnitRecord( studentID, unitCode ).setAsg1(mark);
    }

    /**
     *
     * @return
     */
    @Override
    public float getAsg1() 
    {
        return mngr.getStudentUnitRecord( studentID, unitCode ).getAsg1();
    }
    @Override
    public void setAsg2(float mark) { mngr.getStudentUnitRecord( studentID, unitCode ).setAsg2(mark);}
    
    @Override
    public float getAsg2() {return mngr.getStudentUnitRecord( studentID, unitCode ).getAsg2();}
    
    @Override
    public void setExam(float mark) {mngr.getStudentUnitRecord( studentID, unitCode ).setExam(mark);}

    @Override
    public float getExam() 
    {
        return mngr.getStudentUnitRecord( studentID, unitCode ).getExam();
    }

    @Override
    public float getTotal() {
        return mngr.getStudentUnitRecord( studentID, unitCode ).getTotal();
    }
}