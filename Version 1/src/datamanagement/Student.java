package datamanagement;
//define the student class using IStudent interface
public class Student implements IStudent {
    private Integer id; 
    private String fn;
    private String ln;
    private StudentUnitRecordList su;

    public Student( Integer id, String fn, String ln, StudentUnitRecordList su ) { 
        this.id = id; this.fn = fn;
        this.ln = ln;
        this.su = su == null ? new StudentUnitRecordList() : su;
    }
    //Get student id
    public Integer getID() { return this.id; } 
    //Get Student First name
    public String getFirstName() { return fn; }
    //Set Student First name
    public void setFirstName( String firstName ) { this.fn = firstName; }
    //Get Student Last name
    public String getLastName() { return ln; }
    //Set student Last name
    public void setLastName( String lastName ) { this.ln = lastName; }
    //Add new unti record
    public void addUnitRecord( IStudentUnitRecord record ) { su.add(record); }
    //Get unit record accoring to unit code
    public IStudentUnitRecord getUnitRecord( String unitCode ) {
        for ( IStudentUnitRecord r : su ) 
            if ( r.getUnitCode().equals(unitCode)) 
            return r; 
        return null;        
    }
    public StudentUnitRecordList getUnitRecords() { return su; }
}
